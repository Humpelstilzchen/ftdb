# README

Website for https://ft-datenbank.de/ with Ruby on Rails, Vue.js and vue-good-table

## Import from sfparts
```sh
export IMPORT_ROOT=/home/Downloads/ftdb/import/SFParts-2024-05-10a
bundle exec rake db:importsfpartscsv
```

## Helpful commands
* Dump database to fixtures: `rake db:fixtures:dump`
* Load database from fixtures: `rake db:fixtures:load`
* Recreate OpenAPI definition: `RAILS_ENV=test rails rswag`
* Audit Ruby libraries: `bundle exec bundle audit check --update`
* Audit NPM libraries: `yarn audit`

## Production setup
```sh
export RAILS_ENV=production
export SECRET_KEY_BASE=secret_generated_with_rake_secret
export DATABASE_USER=ftdb
export DATABASE_PORT=5434
export DATABASE_PASSWORD=mysecretdatabasepassword

bundle install
yarn install --check-files --frozen-lockfile
./bin/rails db:migrate
bundle exec rake assets:precompile
```

## License

[MIT License](https://opensource.org/licenses/MIT).
