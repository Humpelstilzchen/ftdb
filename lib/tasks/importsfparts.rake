namespace :db do
	desc "Import SFParts."

	task :importsfpartscsv => :environment do
		require 'open-uri'
		require 'csv'
		require 'open-uri'
		#require 'pry'

		p "Category.."
		Category.destroy_all
		CSV.foreach("#{ENV['IMPORT_ROOT']}/01-Category.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			Category.create!({title: row[2], sfpartsid: row[0], parent: Category.find_by_sfpartsid(row[1])})
		end

		# colors
		I18n.t('.') # initialize translations with dummy
		colorKeyByTrans = I18n.backend.translations[:de][:colors].invert

		colors = {}
		CSV.foreach("#{ENV['IMPORT_ROOT']}/02-Color.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			colors[row[0]] = row[1]
		end

		# check existing colors in enum
		colors_missing = colors.values.uniq - Part::COLORS.map {|c| I18n.backend.translations[:de][:colors][c] }
		raise "Missing colors: " + colors_missing.to_s if colors_missing.length > 0

		p "PartGroup.."
		PartGroup.destroy_all
		CSV.foreach("#{ENV['IMPORT_ROOT']}/03-Article.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			PartGroup.create!({title: row[1], weight: row[3], remarks: ActionView::Base.full_sanitizer.sanitize(row[4]), sfpartsid: row[0], category: Category.find_by_sfpartsid(row[2])})
		end

		document_types = {}
		CSV.foreach("#{ENV['IMPORT_ROOT']}/05-DocumentType.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			document_types[row[0]] = row[2]
		end

		CSV.foreach("#{ENV['IMPORT_ROOT']}/06-ImageType.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			document_types[row[0]] = row[2]
		end

		# check existing languages in enum
		languages = {}
		CSV.foreach("#{ENV['IMPORT_ROOT']}/07-Language.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			lang = row[1]
			lang = "de" if lang == "dt" or lang == "ft"
			languages[row[0]] = lang.to_sym
		end
		langs_missing = languages.values - Part::LANGUAGES
		raise "Missing languages: " + langs_missing.to_s if langs_missing.length > 0

		imageTitles = {}
		CSV.foreach("#{ENV['IMPORT_ROOT']}/08-ImageSubject.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			imageTitles[row[0]] = row[1]
		end

		p "Part.."
		Part.destroy_all
		images = {}
		ignoreimages = []
		CSV.foreach("#{ENV['IMPORT_ROOT']}/09-ArticleVariant.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			color1 = colors[row[3]]
			color2 = colors[row[4]]
			color3 = colors[row[5]]
			Part.create!({part_group: PartGroup.find_by_sfpartsid(row[1]), year: row[2], color1: colorKeyByTrans[color1], color2: colorKeyByTrans[color2], color3: colorKeyByTrans[color3], remarks: row[7], sfpartsid: row[0], ftdb30TicketNumber: row[8], language: languages[row[6]]})
			images[row[0]] = row[13] if !row[13].blank?
			ignoreimages.push(row[11])
		end

		p "PartHistNumber.."
		PartHistNumber.destroy_all
		CSV.foreach("#{ENV['IMPORT_ROOT']}/10-ArticleNumber.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			part = Part.find_by_sfpartsid(row[1])
			part.hist_numbers.build().update!({year: row[2] || 1960, number: row[3] || "-"})
			part.save
		end

		p "PartListEntry.."
		PartListEntry.destroy_all
		CSV.foreach("#{ENV['IMPORT_ROOT']}/11-PartsList.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			PartListEntry.create!({part: Part.find_by_sfpartsid(row[1]), item: Part.find_by_sfpartsid(row[2]), amount: row[3]})
		end

		p "Images.."
		CSV.foreach("#{ENV['IMPORT_ROOT']}/12-Image.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			next if ignoreimages.include? row[0] and !images.has_value? row[0] and row[10].blank?
			part = Part.find_by_sfpartsid(row[1])
			img = URI("https://www.ct-systeme.com:82/ImageHandler.ashx?id=" + row[0]).open # L
			attachment = part.attachments.build()
			#binding.pry if row[0] == "9757362a-65b5-467f-b361-00f6bf1e2939"
			attachment.update({ftdb30TicketNumber: row[10], title: imageTitles[row[3]] || "-"})
			attachment.file.attach(io: img, filename: row[4], content_type: document_types[row[6]])
			attachment.save!
			if images.has_value? row[0]
				part.image = attachment
				part.save!
			end
		end

		p "Documents.."
		CSV.foreach("#{ENV['IMPORT_ROOT']}/13-Document.csv", col_sep: ";", encoding: "UTF-8", headers: true) do |row|
			part = Part.find_by_sfpartsid(row[1])
			doc = URI("https://www.ct-systeme.com:82" + row[8]).open
			attachment = part.attachments.build()
			attachment.update({ftdb30TicketNumber: row[7], title: row[2]})
			attachment.file.attach(io: doc, filename: row[3], content_type: document_types[row[5]])
			attachment.save!
		end
	end
end
