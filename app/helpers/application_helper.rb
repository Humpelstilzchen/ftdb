module ApplicationHelper
	# Returns the full title on a per-page basis.
	def full_title (page_title = '')
		base_title = "ft-Datenbank"
		if page_title.empty?
			base_title
		else
			"#{base_title} | #{page_title}"
		end
	end

	def is_number?(s)
		true if Float(s) rescue false
	end

	def edit_allowed?
		@user&.group == :editor || @user&.group == :admin
	end

	def map_group(jwt_payload)
		return :admin if jwt_payload["groups"]&.include? "/ftdb-admins"
		return :editor if jwt_payload["groups"]&.include? "/ftdb-editoren"
		return :user
	end

	def get_user
		if !request.headers["HTTP_AUTHORIZATION"].blank?
			token = request.headers["HTTP_AUTHORIZATION"].split(" ")[1]
			jwt_public_key = Rails.application.config.respond_to?(:jwt_public_key) ? Rails.configuration.jwt_public_key : nil
			decoded_token = JWT.decode(token, jwt_public_key, !Rails.env.development?, { algorithm: 'RS256' })
			payload = decoded_token[0]
			group = map_group(payload)
			user = User.create_with(group: group).find_or_create_by(username: payload["preferred_username"])
			if user.group != group
				user.group=group
				user.save
			end
			return user
		end
	rescue JWT::ExpiredSignature
		logger.error("JWT Signature expired");
		return nil
	end

	def signed_in_editor?
		if !edit_allowed?
			head :unauthorized
		end
	end

	def signed_in_admin?
		if @user&.group != :admin
			head :unauthorized
		end
	end

	def signed_in_user?
		if @user.nil?
			head :unauthorized
		end
	end
end
