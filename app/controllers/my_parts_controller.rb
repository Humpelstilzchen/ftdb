class MyPartsController < ApplicationController
	before_action :signed_in_user?,  only: [:create]

	def index
		my_parts = {}
		@user&.my_parts.includes(part: [:hist_numbers, part_group: [:category], part_list: [item: [:hist_numbers, part_group: [:category], :image => {file_attachment: :blob}]], :image => {file_attachment: :blob}]).each do |my_part|
			if my_part.part.part_list.size == 0
				# Einzelteil
				if !my_parts.has_key? my_part.part.id
					my_parts[my_part.part.id] = my_part
				else
					my_parts[my_part.part.id].count += my_part.count
				end
				my_parts[my_part.part.id].comment = my_part.comment
			else
				# Baukasten
				my_parts[my_part.part.id] = my_part

				if my_part.count > 0
					my_part.part.part_list.each do |part_list_entry|
						if !my_parts.has_key? part_list_entry.item.id
							my_parts[part_list_entry.item.id] = MyPart.create(part: part_list_entry.item, count: my_part.count*part_list_entry.amount)
						else
							my_parts[part_list_entry.item.id].count += my_part.count*part_list_entry.amount
						end
					end
				end
			end
		end
		@my_parts = my_parts.values
	end

	def create
		create_params = my_part_params
		if create_params.has_key? :count and create_params[:count].blank?
			create_params[:count] = 0
		end
		if create_params[:part_ftdb30TicketNumber]
			create_params[:part_id] = Part.find_by_ftdb30TicketNumber(create_params[:part_ftdb30TicketNumber]).id
			create_params.delete(:part_ftdb30TicketNumber)
		end
		@my_part = @user.my_parts.find_by_part_id(create_params[:part_id])
		if @my_part.nil?
			@my_part = @user.my_parts.build(create_params)
			if @my_part.count.nil?
				@my_part.count = 0
			end
		else
			if create_params.has_key? :count
				@my_part.count = create_params[:count]
			end
			if create_params.has_key? :comment
				@my_part.comment = create_params[:comment]
			end
		end

		if @my_part.count == 0 and @my_part.comment.blank?
			@my_part.destroy
		else
			@my_part.save
		end
		render partial: "my_part", locals: {my_part: @my_part}
	end

	private
		def my_part_params
			params.require(:my_part).permit(:count, :part_id, :part_ftdb30TicketNumber, :comment)
		end
end
