class ApplicationController < ActionController::Base
	include ApplicationHelper
	protect_from_forgery unless: -> { request.format.json? }
	before_action do
		@user = get_user
		if @user&.group == :admin
			Rack::MiniProfiler.authorize_request
		end
	end

	def about
		render "/about"
	end

	# Dummy endpoint to just do nothing
	def token_refresh
		head :ok, content_type: "text/html"
	end
end
