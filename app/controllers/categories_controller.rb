class CategoriesController < ApplicationController
	before_action :signed_in_editor?,  only: [:update]

	def show
		@category = Category.find(params[:id])
	end

	def showsfpartsid
		@category = Category.find_by_sfpartsid(params[:id])
		render :action => 'show'
	end

	def edit
		@category = Category.find(params[:id])
	end

	def update
		@category = Category.find(params[:id])

		if @category.update(category_params)
			flash[:success] = t('flash.update', name: @category.title)
			redirect_to @category
		else
			flash[:danger] = t('flash.error')
			render :edit, status: :unprocessable_entity
		end
	end

	private
		def category_params
			params.require(:category).permit(:remarks, categories_attributes: [:id, :title, :_destroy])
		end
end
