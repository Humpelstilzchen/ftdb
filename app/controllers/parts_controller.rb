class PartsController < ApplicationController
	before_action :signed_in_editor?,  only: [:create, :update, :destroy, :newpartgroup]
	before_action :signed_in_admin?,  only: [:restore]

	def index
		respond_to do |format|
			format.html do
				@categories = Category.where(:parent => nil)
			end
			format.json do
				parts = Part.eager_load(:part_group, :hist_numbers, :part_group => :category, :image => {file_attachment: :blob})
				if !params[:filters].blank?
					JSON.parse(params[:filters]).each do |key, value|
						next if value.blank?
						next if !params[:searchcolums]&.include? key
						value = value.downcase
						case key
						when "part_group.title"
							parts = parts.where("lower(part_groups.title) LIKE ?", "%#{value}%")
						when "current_number"
							parts = parts.where("lower(current_number) LIKE ?", "%#{value}%")
						when "remarks"
							parts = parts.where("lower(parts.remarks) LIKE ?", "%#{value}%")
						when "part_group.remarks"
							parts = parts.where("lower(part_groups.remarks) LIKE ?", "%#{value}%")
						when "colors"
							color = Part::COLORS.index(value.to_sym)
							parts = parts.where("color1 = ? or color2 = ? or color3 = ?", color, color, color)
						when "language"
							lang = Part::LANGUAGES.index(value.to_sym)
							parts = parts.where(:language => lang)
						when "bio"
							parts = parts.where("parts.bio = ?", ActiveModel::Type::Boolean.new.cast(value))
						end
					end
				end
				if !params[:searchtext].blank?
					params[:searchtext].split().each do |s|
						s = s.downcase

						searchCond = Part.where("0=1")
						searchCond = searchCond.or(Part.where('lower(current_number) LIKE ?', "%#{s}%")) if params[:searchcolums]&.include? 'current_number'
						searchCond = searchCond.or(Part.where('lower(part_hist_numbers.number) LIKE ?', "%#{s}%")) if params[:searchcolums]&.include? 'hist_numbers'
						searchCond = searchCond.or(Part.where('lower(part_groups.title) LIKE ?', "%#{s}%")) if params[:searchcolums]&.include? 'part_group.title'
						searchCond = searchCond.or(Part.where('lower(parts.remarks) LIKE ?', "%#{s}%")) if params[:searchcolums]&.include? 'remarks'
						searchCond = searchCond.or(Part.where('lower(part_groups.remarks) LIKE ?', "%#{s}%")) if params[:searchcolums]&.include? 'part_group.remarks'
						parts = parts.merge(searchCond)
					end
				end
				if !params[:category].blank?
					parts = parts.where(:part_groups => {:categories => {:id => Category.find_by_id(params[:category]).all_categories}})
				end
				if params[:size].to_i > 0
					parts = parts.paginate(page: params[:page], per_page: params[:size])
				end
				sortBy0 = case params[:sortBy0]
					when "remarks" then "parts.remarks"
					when "year" then "parts.year"
					when "changedate" then "parts.updated_at"
					when "part_group.title" then "part_groups.title"
					when "part_group.weight" then "part_groups.weight"
					when "part_group.length" then "part_groups.length"
					when "part_group.remarks" then "part_groups.remarks"
					else params[:sortBy0] || "parts.id"
				end
				@parts = params[:sortAsc0].blank? ? parts : parts.order("#{sortBy0} #{params[:sortAsc0] == 'false' ? 'DESC' : 'ASC'}")
				@totalElements = @parts.count
				if @user and !params[:include_my_parts].blank?
					@my_parts = @user.my_parts.includes(:part).where(:part => {id: @parts.pluck(:id)})
				end
			end
		end
	end

	def show
		@part = Part.find(params[:id])
		@category = @part.part_group.category
	end

	def showhist
		part = Part.find(params[:part_id])
		@snapshot, @part = part.histentry(params[:idx].to_i)
		if @part.nil?
			head :not_found
		else
			@category = @part.part_group.category
			render :action => 'show'
		end
	end

	def overlay
		@part = Rails.cache.fetch ["parts_overlay", Part.all.cache_key, params[:part_id]], expirese: 1.hour do
			Part.find(params[:part_id])
		end
		render :layout => "overlay"
	end

	def partlist
		@partlist = Part.find(params[:part_id]).part_list.eager_load(:item => [:part_group, :hist_numbers, :part_group => :category, :image => {file_attachment: :blob}])
	end

	def restore
		part = Part.find(params[:part_id])
		snapshot = part.snapshots[params[:idx].to_i]
		snapshot.restore!
		redirect_to part_path(part)
	end

	def partlisthist
		part = Part.find(params[:part_id])
		@snapshot, @part = part.histentry(params[:idx].to_i)
		@partlist = @part.part_list
		render :action => 'partlist'
	end

	def partlistFtdb30TicketNumber
		@partlist = Part.find_by_ftdb30TicketNumber(params[:id]).part_list
		render :action => 'partlist'
	end

	def contained
		@partlist = Part.find(params[:part_id]).contained.eager_load(:part => [:part_group, :hist_numbers, :part_group => :category, :image => {file_attachment: :blob}])
	end

	def variants
		@variants = Part.find(params[:part_id]).part_group.variants.eager_load(:part_group, :hist_numbers, :part_group => :category, :image => {file_attachment: :blob})
	end

	def showsfpartsid
		@part = Part.find_by_sfpartsid(params[:id] || params[:ArticleVariantId])
		@category = @part.part_group.category
		render :action => 'show'
	end

	def showFtdb30TicketNumber
		@part = Part.find_by_ftdb30TicketNumber(params[:id])
		@category = @part.part_group.category
		render :action => 'show'
	end

	def attachmentFtdb30TicketNumber
		@attachment = Attachment.find_by_ftdb30TicketNumber(params[:id])
		redirect_to @attachment.file
	end

	def new
		@part = Part.find(params[:id]).part_group.variants.build
		@part.hist_numbers.build
		@part.attachments.build
		@category = @part.part_group.category
		100.times { |i| @part.part_list.build }
	end

	def newpartgroup
		@category = Category.find(params[:part_id])
		part_group = @category.part_groups.build
		@part = part_group.variants.build
		@part.hist_numbers.build
		@part.attachments.build
		100.times { |i| @part.part_list.build }
		render :new
	end

	def create
		params = part_params
		group_params = params.delete(:part_group_attributes)
		part_group = !group_params[:id].blank? ? PartGroup.find(group_params[:id]) : Category.find(group_params[:category_id]).part_groups.build(group_params)
		@part = part_group.variants.build(params)
		@category = @part.part_group.category

		if @part.save
			@part.create_snapshot!(user: @user)
			flash[:success] = t('flash.update', name: @part.title)
			redirect_to @part
		else
			flash[:danger] = t('flash.error')
			render :new, status: :unprocessable_entity
		end
	end

	def edit
		@part = Part.find(params[:id])
		@category = @part.part_group.category
		if @part.hist_numbers.size == 0
			@part.hist_numbers.build
		end
		if @part.attachments.size == 0
			@part.attachments.build
		end
	end

	def update
		@part = Part.find(params[:id])

		if @part.update(part_params)
			@part.create_snapshot!(user: @user)
			flash[:success] = t('flash.update', name: @part.title)
			redirect_to @part
		else
			flash[:danger] = t('flash.error')
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		@part = Part.find(params[:id])
		part_group = @part.part_group
		category = part_group.category
		@part.destroy
		MyPart.where(:part_id => params[:id]).destroy_all
		if part_group.variants.count == 0
			part_group.destroy
		end
		flash[:success] = t('flash.destroy', name: @part.title)

		redirect_to category_path(category), status: :see_other
	end

	private
		def part_params
			params.require(:part).permit(:year, :color1, :color2, :color3, :language, :remarks, :image, :bio, hist_numbers_attributes: [:id, :number, :year, :_destroy], attachments_attributes: [:id, :title, :file, :_destroy], part_list_attributes: [:id, :_destroy, :amount, :item_id], part_group_attributes: [:id, :title, :weight, :remarks, :category_id, :length])
		end
end
