json.partial! 'parts/part', part: my_part.part
json.my_part do
	json.count my_part.count
	json.comment my_part.comment
end
