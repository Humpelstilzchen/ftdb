json.array! @variants do |part|
	json.partial! 'part', part: part
end
