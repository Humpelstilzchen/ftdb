json.content do
	json.array! @parts do |part|
		json.partial! 'part', part: part
		if defined? @my_parts
			json.my_part do
				my_part = @my_parts.find{|p| p.part.id == part.id}
				json.count my_part&.count
				json.comment my_part&.comment
			end
		end
	end
end
json.totalElements @totalElements
