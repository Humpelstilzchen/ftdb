json.array! @partlist do |part|
	json.amount part.amount
	json.partial! 'part', part: part.part
end

