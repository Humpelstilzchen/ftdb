json.id part.id
json.current_number part.current_number
json.bio part.bio
json.color1 part.color1
json.color2 part.color2
json.color3 part.color3
json.colors part.colors
json.language part.language
json.year part.year
json.sfpartsid part.sfpartsid
json.changedate part.updated_at
json.hist_numbers do
	json.array! part.hist_numbers do |histnum|
		json.number histnum.number
		json.year histnum.year
	end
end
json.remarks part.remarks
json.part_group do
	json.id part.part_group.id
	json.title part.part_group.title
	json.remarks part.part_group.remarks
	json.weight part.part_group.weight
	json.length part.part_group.length
	json.category do
		json.id part.part_group.category&.id
		json.sfpartsid part.part_group.category&.sfpartsid
		json.full_title part.part_group.category&.full_title
	end
end
json.ftdb30TicketNumber part.ftdb30TicketNumber
if part.image
	json.image url_for(part.image.file)
end
