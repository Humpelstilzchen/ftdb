# == Schema Information
#
# Table name: part_groups
#
#  id          :integer          not null, primary key
#  title       :string
#  weight      :float
#  remarks     :text
#  sfpartsid   :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  length      :float
#
class PartGroup < ApplicationRecord
	belongs_to :category
	has_many :variants, -> { order(:current_number => :asc)}, :class_name => "Part", dependent: :destroy

	validates :title, presence: true
end
