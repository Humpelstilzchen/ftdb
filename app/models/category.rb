# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  title      :string
#  parent_id  :integer
#  sfpartsid  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  remarks    :text
#
class Category < ApplicationRecord
	belongs_to :parent, :class_name => 'Category', inverse_of: :categories, optional: true
	has_many :categories, :class_name => 'Category', inverse_of: :parent, index_errors: :nested_attributes_order
	has_many :part_groups

	default_scope { order(title: :asc) }

	accepts_nested_attributes_for :categories, :allow_destroy => true

	validates :title, presence: true, uniqueness: { scope: :parent_id }

	def all_parents
		l = []
		category = self
		while category != nil do
			l.append(category)
			category = category.parent
		end
		return l.reverse
	end

	def full_title
		Rails.cache.fetch ["category_fulltitle", cache_key_with_version] do
			all_parents.map{|cat| cat.title}.join(" / ")
		end
	end

	def all_categories
		Rails.cache.fetch ["all_categories", cache_key_with_version] do
			cats = []

			cats << self
			categories.each do |cat|
				cats.concat(cat.all_categories())
			end

			return cats
		end
	end

	def url_show
		return {controller: "categories", action: "showsfpartsid", id: sfpartsid} if !sfpartsid.nil?
		return {controller: "categories", action: "show", id: id}
	end

	def destroy
		raise "Cannot delete category with subcategories" unless categories.count == 0
		raise "Cannot delete category with parts" unless part_groups.count == 0
		super
	end
end
