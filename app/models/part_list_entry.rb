# == Schema Information
#
# Table name: part_list_entries
#
#  id         :integer          not null, primary key
#  part_id    :integer
#  item_id    :integer
#  amount     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class PartListEntry < ApplicationRecord
	belongs_to :part, :class_name => 'Part'
	belongs_to :item, :class_name => 'Part'

	validates :amount, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, presence: true
end
