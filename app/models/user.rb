# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  username   :string
#  group      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class User < ApplicationRecord
	GROUPS = [:user, :editor, :admin]

	has_many :my_parts
	has_many :parts, through: :my_parts

	validates :username, presence: true
	validates :group, presence: true

	def group
		GROUPS[read_attribute(:group)]
	end

	def group= (value)
		write_attribute(:group, GROUPS.index(value.to_sym))
	end
end
