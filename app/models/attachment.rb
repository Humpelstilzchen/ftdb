# == Schema Information
#
# Table name: attachments
#
#  id                 :integer          not null, primary key
#  part_id            :integer
#  ftdb30TicketNumber :string
#  title              :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class Attachment < ApplicationRecord
	belongs_to :part
	has_one_attached :file

	validates :title, presence: true
	validate :file_validation

	private
		def file_validation
			if !file.attached?
				errors.add(:file, I18n.t('errors.messages.blank'))
				false
			else
				if !file.content_type.starts_with?('image/') && !['application/pdf', 'video/mp4', 'application/zip', 'video/x-flv', 'video/x-msvideo'].include?(file.content_type)
					file.purge
					errors.add(:file, I18n.t('errors.messages.file_type'))
					false
				else
					true
				end
			end
		end
end
