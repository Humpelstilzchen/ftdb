# == Schema Information
#
# Table name: part_hist_numbers
#
#  id         :integer          not null, primary key
#  part_id    :integer
#  year       :integer
#  number     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class PartHistNumber < ApplicationRecord
	belongs_to :part

	validates :year, numericality: { only_integer: true, greater_than_or_equal_to: 1960, allow_nil: true }, presence: true
	validate :uniqueness_validation

	private
		def uniqueness_validation
			if !number.blank? and PartHistNumber.where(number: number).where(year: year).where.not(id: id).exists?
				errors.add(:number, I18n.t('errors.messages.hist_num_uniqueness'))
			end
		end
end
