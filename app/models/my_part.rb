# == Schema Information
#
# Table name: my_parts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  part_id    :integer
#  count      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  comment    :text
#
class MyPart < ApplicationRecord
	belongs_to :user
	belongs_to :part

	validates :user, presence: true
	validates :part, presence: true
	validates :count, presence: true
end
