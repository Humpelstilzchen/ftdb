# == Schema Information
#
# Table name: parts
#
#  id                 :integer          not null, primary key
#  part_group_id      :integer
#  year               :integer
#  color1             :integer
#  color2             :integer
#  language           :integer
#  remarks            :text
#  current_number     :string
#  sfpartsid          :string
#  ftdb30TicketNumber :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_id           :integer
#  color3             :integer
#  bio                :boolean          default(FALSE)
#
class Part < ApplicationRecord
	include ApplicationHelper
	include ActiveSnapshot

	COLORS = [:black, :green, :pink, :silver, :white, :red, :purple, :orange, :brown, :transparent, :blue, :grey, :yellow, :metallic, :chrome, :greywhite]
	LANGUAGES = [:de, :ru, :en, :nl, :es, :pt, :fr, :ju, :ml, :it]

	belongs_to :part_group
	has_many :hist_numbers, -> { order(year: :asc) }, :class_name => 'PartHistNumber', dependent: :destroy, index_errors: :nested_attributes_order
	has_many :part_list, -> { includes(:item).order('parts.current_number') }, :class_name => 'PartListEntry', dependent: :destroy, index_errors: :nested_attributes_order
	has_many :contained, :class_name => 'PartListEntry', inverse_of: :item
	has_many :attachments, dependent: :destroy, index_errors: :nested_attributes_order
	belongs_to :image, :class_name => 'Attachment', optional: true

	accepts_nested_attributes_for :hist_numbers, :allow_destroy => true, reject_if: :all_blank
	accepts_nested_attributes_for :attachments, :allow_destroy => true, reject_if: :all_blank
	accepts_nested_attributes_for :part_list, :allow_destroy => true, reject_if: :all_blank
	accepts_nested_attributes_for :part_group

	validates :year, numericality: { only_integer: true, greater_than_or_equal_to: 1960, allow_nil: true }
	validate :color_validation

	before_save :before_save

	has_snapshot_children do
		### Executed in the context of the instance / self

		### Reload record from database to ensure a clean state and eager load the specified associations
		instance = self.class.includes(:part_group, :hist_numbers, :part_list, :attachments).find(id)

		### Define the associated records that will be restored
		{
			part_group: instance.part_group,
			hist_numbers: instance.hist_numbers,
			part_list: instance.part_list,
			attachments: instance.attachments
		}
	end

	def color1
		COLORS[read_attribute(:color1)] if !read_attribute(:color1).nil?
	end

	def color1= (value)
		write_attribute(:color1, COLORS.index(value&.to_sym))
	end

	def color2
		COLORS[read_attribute(:color2)] if !read_attribute(:color2).nil?
	end

	def color2= (value)
		write_attribute(:color2, COLORS.index(value&.to_sym))
	end

	def color3
		COLORS[read_attribute(:color3)] if !read_attribute(:color3).nil?
	end

	def color3= (value)
		write_attribute(:color3, COLORS.index(value&.to_sym))
	end

	def language
		LANGUAGES[read_attribute(:language)] if !read_attribute(:language).nil?
	end

	def language= (value)
		write_attribute(:language, LANGUAGES.index(value&.to_sym))
	end

	def update_number
		self.current_number = self.hist_numbers.last&.number # assuming always in past, so current is last
	end

	def update_year
		self.year = self.hist_numbers.first&.year
	end

	def colors
		[color1, color2, color3].compact.uniq
	end

	def title
		ret = part_group.title
		ret += " #{colors.map { |color| I18n.t(color, scope: "colors")}.join('/') }" if !colors.blank?
		ret += " #{I18n.t(language, scope: "languages")}" if !language.blank?
		ret += " #{I18n.t("bio", scope: "part")}".downcase if bio
		ret += " (#{current_number})" if !current_number.blank?
		return ret
	end

	def image= (value)
		if is_number? value
			value = self.attachments[value.to_i]
		end
		super(value)
	end

	def destroy
		raise "Cannot delete part with partlist" unless contained.count == 0
		super
	end

	def histentry(idx)
		snapshot = snapshots[idx]
		part, reified_children_hash = snapshot.fetch_reified_items
		ActiveRecord::Base.transaction do
			if !part.nil?
				part.part_group = reified_children_hash["part_group"]&.dig(0)
				part.hist_numbers = reified_children_hash["hist_numbers"] || []
				part.part_list = reified_children_hash["part_list"] || []
				part.attachments = reified_children_hash["attachments"] || []
				before_save
				raise ActiveRecord::Rollback
			end
		end
		return snapshot, part
	end

	private
		def before_save
			update_number
			update_year
			if self.image.nil?
				# if only one image attached set this as default
				images = self.attachments.filter { |att| att.file.content_type.starts_with?('image/')}
				if images.length == 1
					self.image = images[0]
				end
			end
		end

		def color_validation
			if color1.nil? && !color2.nil?
				errors.add(:color2, I18n.t('errors.messages.color_gap1'))
			end
			if color2.nil? && !color3.nil?
				errors.add(:color3, I18n.t('errors.messages.color_gap2'))
			end
			if !color2.nil? && color2 == color1
				errors.add(:color2, I18n.t('errors.messages.color_uniq'))
			end
			if !color3.nil? && (color3 == color1 || color3 == color2)
				errors.add(:color3, I18n.t('errors.messages.color_uniq'))
			end
		end
end
