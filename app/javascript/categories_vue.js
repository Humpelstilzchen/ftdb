import { createApp } from 'vue/dist/vue.esm-bundler';
import { marked } from 'marked';

document.addEventListener('DOMContentLoaded', () => {
	const app = createApp({
		methods: {
			formatMarkdown(s) {
				return s ? marked.parse(s) : s;
			},
		},
	});
	app.mount('#category_app')
})
