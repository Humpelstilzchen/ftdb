import { createApp } from 'vue/dist/vue.esm-bundler';
import VueGoodTablePlugin from 'vue-good-table-next';
import 'vue-good-table-next/dist/vue-good-table-next.css'
const csvConverter = require('json-2-csv');
import FileSaver from 'file-saver';
import { marked } from 'marked';

document.addEventListener('DOMContentLoaded', () => {
	const app = createApp({
		data() {
			const hidden_columns = getPartListHiddenColums();
			const common_columns = [
				{ label: i18n.t('part.image'), field: 'image', sortable: false, hidden: hidden_columns['image'], width: '100px', tdClass: 'text-center' },
				{ label: i18n.t('part.title'), field: 'part_group.title', hidden: hidden_columns['part_group.title'] },
				{ label: i18n.t('part.current_number'), field: 'current_number', type: 'number', hidden: hidden_columns['current_number'] },
				{ label: i18n.t('part.hist_numbers', { count: 2 }), field: 'hist_numbers', sortable: false, hidden: hidden_columns['hist_numbers'], formatFn: (artvar) => artvar.map(el => `${el.number || "-"} (${el.year})`).join(", ") },
				{ label: i18n.t('part.year'), field: 'year', type: 'number', hidden: hidden_columns['year'] },
				{ label: i18n.t('part.remarks'), field: 'remarks', width: '50ch', hidden: hidden_columns['remarks']},
				{ label: i18n.t('part_group.remarks'), field: 'part_group.remarks', width: '50ch', hidden: hidden_columns['part_group.remarks']},
				{ label: i18n.t('part.weight'), field: 'part_group.weight', type: 'number', hidden: hidden_columns['part_group.weight'], formatFn: weight => weight?.toLocaleString() },
				{ label: i18n.t('part.colors'), field: 'colors' },
				{ label: i18n.t('part.bio'), field: 'bio', type: 'boolean', hidden: hidden_columns['bio'], formatFn: bio => bio != null ? i18n.t(bio.toString(), {scope: "bio"}) : null },
				{ label: i18n.t('part.language'), field: 'language', hidden: hidden_columns['language'], formatFn: lang => lang != null ? i18n.t(lang, {scope: "languages"}) : null  },
				{ label: i18n.t('part.length'), field: 'part_group.length', type: 'number', hidden: hidden_columns['part_group.length'], formatFn: length => length?.toLocaleString() },
			];
			const initialSort = JSON.parse(getLocalStorage("parts_sort", '{"field": "current_number", "type": "asc"}'));
			return {
				common_columns: common_columns,
				part_list_columns: [
					...common_columns,
					{ label: i18n.t('part_list_entry.amount'), field: 'amount', type: 'number'},
					{ label: i18n.t('part.articleCounts'), field: "mypartcount", hidden: true, type: 'number'},
				],
				variants_columns: common_columns,
				contained_columns: [
					{ label: i18n.t('part_list_entry.amount'), field: 'amount', type: 'number'},
					...common_columns,
				],
				part_list_rows: [],
				variants_rows: [],
				contained_rows: [],
				searchOptions: {
					enabled: true,
					placeholder: i18n.t('part.search.description'),
				},
				sortOptions: initialSort,
				contained_sortOptions: {field: 'amount', type: 'desc'},
				isLoading: true,
				mypartsloaded: false,
			}
		},
		created() {
			this.fetchPartList();
			this.fetchVariants();
			this.fetchContained();
		},
		mounted() {
			$(".myComment").autocomplete({
				source: [...document.querySelectorAll("#myCommentOptions option")].map(opt => opt.value),
			});
			const articleCountsSingle = document.getElementById('my_part_single_count')?.value;
			if (articleCountsSingle !== undefined) {
				this.updateTotalArticleCounts(parseInt(articleCountsSingle));
			}
		},
		methods: {
			fetchPartList() {
				const url = new URL(window.location.href);
				url.pathname = `/parts/${document.getElementById('app').dataset.partid}/partlist`
				if (document.getElementById('app').dataset.histidx) {
					url.pathname+= `hist/${document.getElementById('app').dataset.histidx}`;
				}
				fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
					this.part_list_rows = data;
					this.isLoading = false;
				});
			},
			fetchVariants() {
				const url = new URL(window.location.href);
				url.pathname = `/parts/${document.getElementById('app').dataset.partid}/variants`
				fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
					this.variants_rows = data;
					this.isLoading = false;
				});
			},
			fetchContained() {
				const url = new URL(window.location.href);
				url.pathname = `/parts/${document.getElementById('app').dataset.partid}/contained`
				fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
					this.contained_rows = data;
					this.isLoading = false;
				});
			},
			toggleColumn(index, event) {
				this.common_columns[index].hidden = !this.common_columns[index].hidden;
				const hidden_columns = this.common_columns.reduce((acc, col) => {acc[col.field] = !!col.hidden; return acc;}, {});
				window.localStorage.setItem('parts_hidden_columns', JSON.stringify(hidden_columns));
			},
			downloadCsv(rows, filename) {
				csvConverter.json2csv(rows, (err, csv) => {
					if (err) {
						console.error(err);
					} else {
						const blob = new Blob([csv], {type: "text/plain;charset=utf-8"});
						saveAs(blob, filename);
					}
				}, {expandArrayObjects: true, emptyFieldValue: ''});
			},
			downloadPartListCsv() {
				this.downloadCsv(this.part_list_rows, `partlist_${document.getElementById('app').dataset.parttitle}_export.csv`);
			},
			downloadVariantsCsv() {
				this.downloadCsv(this.variants_rows, `variants_${document.getElementById('app').dataset.parttitle}_export.csv`);
			},
			downloadContainedCsv() {
				this.downloadCsv(this.contained_rows, `contained_${document.getElementById('app').dataset.parttitle}_export.csv`);
			},
			showPartsPopover(id) {
				showPartsPopover(id);
			},
			hidePartsPopover() {
				hidePartsPopover();
			},
			t(scope, options) {
				return i18n.t(scope, options);
			},
			getColorBg(color) {
				return COLOR_MAP[color]?.bg || color;
			},
			getColorFg(color) {
				return COLOR_MAP[color]?.fg || "#fff";
			},
			mycommentChanged(event) {
				fetch('/my_parts', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify({my_part: {part_id: event.target.dataset.id, comment: event.target.value}})}).catch(error => {
					alert("Failed to save!");
				});
			},
			myAmountChanged(event) {
				const articleCountsSingle = parseInt(event.target.value);
				fetch('/my_parts', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify({my_part: {part_id: event.target.dataset.id, count: articleCountsSingle}})}).catch(error => {
					alert("Failed to save!");
				});
				this.updateTotalArticleCounts(articleCountsSingle);
			},
			updateTotalArticleCounts(articleCountsSingle) {
				const my_part_included_count = parseInt(document.getElementById('my_part_included_count').value);
				const articleCountsTotal = my_part_included_count + articleCountsSingle;
				document.getElementById('my_part_total_count').value = articleCountsTotal;
			},
			formatMarkdown(s) {
				return s ? marked.parse(s) : s;
			},
			matchMyParts() {
				this.isLoading = true;
				const url = new URL(window.location.href);
				url.pathname = `/my_parts`
				fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
					this.part_list_rows.forEach(part => {
						part.mypartcount = data.filter(mypart => mypart.id == part.id)[0]?.my_part.count || 0;
					});
					this.part_list_columns.filter(col => col.field == "mypartcount")[0].hidden = false; 
					this.isLoading = false;
					this.mypartsloaded = true;
				});
			},
			partlistRowStyleClass(row) {
				if (this.mypartsloaded) {
					return row.mypartcount >= row.amount ? 'partlistok' : 'partlistmissing';
				}
			},
		},
	});
	app.use(VueGoodTablePlugin);
	app.mount('#app')
})
