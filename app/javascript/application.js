// Entry point for the build script in your package.json

window.$ = window.jQuery = require('jquery');
require("jquery-ui/ui/widgets/autocomplete");

import Rails from "@rails/ujs"
import * as ActiveStorage from "@rails/activestorage"
import "./channels"

require("@popperjs/core")
import { Popover } from 'bootstrap';

import { I18n } from "i18n-js";
import translations from "./translations.json";

import 'select2';
import { marked } from 'marked';

// https://www.npmjs.com/package/i18n-js
window.i18n = new I18n(translations);
window.i18n.locale = document.documentElement.lang;
window.i18n.missingBehavior = "guess";

Rails.start()
ActiveStorage.start()

window.getLocalStorage = (key, defaultValue) => {
	try {
		return window.localStorage.getItem(key) || defaultValue;
	} catch(err) {
		console.error(err);
	}
	return defaultValue;
}

window.getPartListHiddenColums = () => {
	return {
		...{
			'current_number': true,
			'remarks': true,
			'part_group.remarks': true,
			'remarks': true,
			'part_group.weight': true,
			'language': true,
			'part_group.length': true,
			'year': true,
			'bio': true,
		},
		...JSON.parse(getLocalStorage('parts_hidden_columns', "{}")),
	};
}

let partsPopoverShow = false;
let partsPopoverContent;
let partsPopoverTitle;
let partsPopover;
let mouse_x,mouse_y;
// https://webaim.org/resources/contrastchecker/
window.COLOR_MAP = {
	'pink': {fg: "black"},
	'silver': {fg: "black"},
	'white': {fg: "black"},
	'orange': {fg: "black"},
	'transparent': {fg: "black", bg: "#f0f0f0"},
	'yellow': {fg: "#000"},
	'metallic': {fg: "black", bg: "#b6b6b6"},
	'chrome': {fg: "black", bg: "#d8dbde"},
	'greywhite': {fg: "black", bg: "#efefef"}
};

window.showPartsPopover = (id) => {
	partsPopoverShow = true;
	const url = new URL(window.location.href);
	url.pathname = `/parts/${id}/overlay`
	partsPopoverTitle = null;
	partsPopoverContent = null;
	fetch(url.href, {cache: "force-cache"}).then(response => response.text()).then(data => {
		const parser = new DOMParser();
		const overlay = parser.parseFromString(data, "text/html");
		const a = document.querySelector("tr > :hover a");
		const rowpopover = document.querySelector('#rowpopover');
		if (partsPopoverShow && overlay.querySelector("#body") && a) {
			const y = window.pageYOffset + a.getBoundingClientRect().bottom; // position on lower edge of current row
			rowpopover.style.setProperty("left", `${mouse_x}px`);
			rowpopover.style.setProperty("top", `${y}px`);
			partsPopoverTitle = overlay.querySelector("#title").innerHTML;
			partsPopoverContent = overlay.querySelector("#body").innerHTML;
			partsPopover.show();
		} else {
			hidePartsPopover();
		}
	}).catch(error => {
		console.error("Failed to fetch", error);
		hidePartsPopover();
	});
};

window.hidePartsPopover = () => {
	partsPopoverShow = false;
	partsPopover.hide();
};

const partlistPartsTemplate = (repo) => {
	var $container = $(
	  "<div class='select2-result-repository clearfix'>" +
	    "<div class='select2-result-repository__image'><img src='" + (repo.image || '') + "' style='max-width: 96px; max-height: 96px;'/></div>" +
	    "<div class='select2-result-repository__meta'>" +
	      "<div class='select2-result-repository__title'></div>" +
	      "<div class='select2-result-repository__description'></div>" +
	      "<div class='select2-result-repository__statistics'>" +
		"<div class='select2-result-repository__cat'></div>" +
	      "</div>" +
	    "</div>" +
	  "</div>"
	);
	$container.find(".select2-result-repository__title").text(repo.current_number);
	const color = repo.color1 ? i18n.t(repo.color1, {scope: "colors"}) : '';
	$container.find(".select2-result-repository__description").text(repo.part_group.title + ' ' + color);
	$container.find(".select2-result-repository__cat").append(repo.part_group.category.full_title);
	return $container;
};

window.loadParlistPartsSelect = () => {
	const urlParts = new URL(window.location.href);
	urlParts.pathname = "/parts"
	$('.select2-partlist-part').each((idx, element) => {
		if ($(element).hasClass("select2-hidden-accessible")) {
			return; // already initialized
		}
		$(element).select2({
			ajax: {
				url: urlParts.href,
				dataType: 'json',
				data: (params) => {
					return {
						searchtext: params.term,
						searchcolums: JSON.stringify(["part_group.title", "current_number"])
					};
				},
				cache: true,
				processResults: (data, params) => {
					return {results: data.content};
				},
			},
			minimumInputLength: 2,
			templateResult: (repo) => {
				if (repo.loading) {
					return repo.text;
				}
				return partlistPartsTemplate(repo);
			},
			templateSelection: (repo) => {
				try {
					repo = JSON.parse(repo.text);
				} catch (e) {}
				if (repo.part_group) {
					return partlistPartsTemplate(repo);
				}
				return repo.text;
			},
		});
	});
};

document.addEventListener('DOMContentLoaded', () => {
	const rowpopover = document.querySelector('#rowpopover');
	partsPopover = new Popover(rowpopover, {
		container: 'body',
		trigger: 'manual',
		html: true,
		offset: [0, 30],
		animation: false,
		title: () => partsPopoverTitle,
		content: () => partsPopoverContent,
		placement: 'bottom',
	});

	document.addEventListener('mousemove', ({ pageX, pageY, clientX, clientY }) => {
		mouse_x = pageX;
		mouse_y = pageY;
	});

	loadParlistPartsSelect();
	$(".part_list .add_fields").on("click", function() {
		loadParlistPartsSelect();
	});
	document.querySelectorAll('.select2-partlist-part').forEach(el => {
		const id = el.options[el.options.selectedIndex]?.value;

		if (id) {
			const url = new URL(window.location.href);
			url.pathname = `/parts/${id}`
			fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
				const select2 = $(el);
				// Workaround: pass the data as json in the text attribute to be able to render the template with it
				const option = new Option(JSON.stringify(data), id, true, true);
				select2.empty().append(option).trigger('change');
			});
		}
	});
	$('select.select2').select2();

	const titleEl = document.getElementsByName("part[part_group_attributes][title]")[0];
	const gidEl = document.getElementsByName("part[part_group_attributes][id]")[0]?.value;
	if (titleEl) {
		titleEl.onblur = (event) => {
			document.querySelector(".titleExistingCheck")?.remove()
			const url = new URL(window.location.href);
			url.pathname = '/parts'
			url.searchParams.set("filters", JSON.stringify({"part_group.title": event.target.value}));
			fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
				if (data.content.filter(part => part.part_group.title === event.target.value && part.part_group.id != gidEl).length > 0) {
					const divNode = document.createElement("div");
					divNode.classList.add("titleExistingCheck");
					divNode.classList.add("text-warning");
					divNode.textContent = i18n.t('existsGroupTitle', {title: event.target.value});
					titleEl.parentElement.appendChild(divNode);
				}
			});
		};
	}
});

const username = document.querySelector("body").dataset.user;
if (username) {
	setInterval(() => {
		// stay logged in
		fetch('/token_refresh');
	}, 10*60*1000);
}

import "./controllers"
