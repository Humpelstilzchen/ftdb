import { createApp } from 'vue/dist/vue.esm-bundler';
import VueGoodTablePlugin from 'vue-good-table-next';
import 'vue-good-table-next/dist/vue-good-table-next.css'
const csvConverter = require('json-2-csv');
import FileSaver from 'file-saver';
import { marked } from 'marked';

document.addEventListener('DOMContentLoaded', () => {
	const app = createApp({
		data() {
			const hidden_columns = getPartListHiddenColums();
			const rowsPerPage = parseInt(getLocalStorage('parts_row_num', '20'));
			const initialSort = JSON.parse(getLocalStorage("parts_sort", '{"field": "current_number", "type": "none"}'));
			const initialFiltersEnabled = getLocalStorage('parts_filters_enabled', 'false') === 'true';
			const colors = JSON.parse(document.getElementById('app').dataset.colors);
			const languages = JSON.parse(document.getElementById('app').dataset.languages);
			return {
				rows: [],
				initial_rows: [],
				columns: [
					{ label: i18n.t('part.image'), field: 'image', sortable: false, hidden: hidden_columns['image'], width: '100px', tdClass: 'text-center' },
					{ label: i18n.t('part.title'), field: 'part_group.title', hidden: hidden_columns['part_group.title'], filterOptions: {enabled: initialFiltersEnabled} },
					{ label: i18n.t('part.current_number'), field: 'current_number', type: 'number', hidden: hidden_columns['current_number'], filterOptions: {enabled: initialFiltersEnabled, filterFn: (data, filterString) => {return data?.includes(filterString);}} },
					{ label: i18n.t('part.hist_numbers', { count: 2 }), field: 'hist_numbers', sortable: false, hidden: hidden_columns['hist_numbers'], formatFn: (artvar) => artvar.map(el => `${el.number || "-"} (${el.year})`).join(", ") },
					{ label: i18n.t('part.year'), field: 'year', type: 'number', hidden: hidden_columns['year'] },
					{ label: i18n.t('part.remarks'), field: 'remarks', width: '50ch', hidden: hidden_columns['remarks'], filterOptions: {enabled: initialFiltersEnabled} },
					{ label: i18n.t('part_group.remarks'), field: 'part_group.remarks', width: '50ch', hidden: hidden_columns['part_group.remarks'], filterOptions: {enabled: initialFiltersEnabled} },
					{ label: i18n.t('part.weight'), field: 'part_group.weight', type: 'number', hidden: hidden_columns['part_group.weight'], formatFn: weight => weight?.toLocaleString() },
					{ label: i18n.t('part.colors'), field: 'colors', filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: colors.map(color => ({value: color, text: i18n.t(color, {scope: "colors"})})).sort((a, b) => {return a.text.localeCompare(b.text)})} },
					{ label: i18n.t('part.bio'), field: 'bio', type: 'boolean', filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: [{value: "true", text: i18n.t("true", {scope: "bio"})}, {value: "false", text: i18n.t("false", {scope: "bio"})}]}, hidden: hidden_columns['bio'], formatFn: bio => bio != null ? i18n.t(bio.toString(), {scope: "bio"}) : null },
					{ label: i18n.t('part.language'), field: 'language', hidden: hidden_columns['language'], formatFn: lang => lang != null ? i18n.t(lang, {scope: "languages"}) : null, filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: languages.map(lang => ({value: lang, text: i18n.t(lang, {scope: "languages"})})).sort((a, b) => {return a.text.localeCompare(b.text)})} },
					{ label: i18n.t('part.length'), field: 'part_group.length', type: 'number', hidden: hidden_columns['part_group.length'], formatFn: length => length?.toLocaleString() },
					{ label: i18n.t('part.articleCounts'), field: 'my_part.count', type: 'number' },
					{ label: i18n.t('part.mycomment'), field: 'my_part.comment', sortable: false },
				],
				searchOptions: {
					enabled: true,
					placeholder: i18n.t('part.search.description'),
				},
				sortOptions: initialSort,
				myparts: {},
				isLoading: true,
				paginationOptions: {
					enabled: true,
					perPage: rowsPerPage,
					mode: 'pages',
					dropdownAllowAll: false,
					jumpFirstOrLast: true,
					perPageDropdown: [10, 20, 50, 100],
				},
				filtersEnabled: initialFiltersEnabled,
				myCommentOptions: [...document.querySelectorAll("#myCommentOptions option")].map(opt => opt.value),
			}
		},
		created() {
			const url = new URL(window.location.href);
			url.pathname = `/my_parts`
			fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
				this.initial_rows = data;
				this.update_rows();
				this.isLoading = false;
			});
		},
		updated() {
			$(".myComment").autocomplete({
				source: this.myCommentOptions
			});
			$(".myComment").removeClass("myComment"); // init only once
		},
		methods: {
			update_rows() {
				if (this.isGrouping()) {
					// Group result by category
					const contentByCat = this.initial_rows.reduce((acc,row) => {
						(acc[row.part_group.category.full_title] = acc[row.part_group.category.full_title] || []).push(row);
						return acc;
					}, {})
					const content = Object.keys(contentByCat).map((key) => ({
						mode: 'span',
						label: key,
						children: contentByCat[key],
					}));
					this.rows = content;
				} else {
					this.rows = this.initial_rows;
				}
			},
			toggleColumn(index, event) {
				this.columns[index].hidden = !this.columns[index].hidden;
				const hidden_columns = this.columns.reduce((acc, col) => {acc[col.field] = !!col.hidden; return acc;}, {});
				window.localStorage.setItem('parts_hidden_columns', JSON.stringify(hidden_columns));
			},
			downloadCsv() {
				csvConverter.json2csv(this.rows, (err, csv) => {
					if (err) {
						console.error(err);
					} else {
						const blob = new Blob([csv], {type: "text/plain;charset=utf-8"});
						saveAs(blob, "myparts_export.csv");
					}
				}, {expandArrayObjects: true, emptyFieldValue: '', keys: ['id', {field: 'current_number', title: 'number'}, {field: 'part_group.title', title: 'title'}, {field: 'part_group.category.full_title', title: 'category'}, 'year', 'color1', 'color2', 'color3', 'language', 'remarks', {field: 'part_group.weight', title: 'weight'}, {field: 'part_group.length', title: 'length'}, {field: 'part_group.remarks', title: 'remarks (Group)'}, {field: 'my_part.count', title: 'count'}]});
			},
			showPartsPopover(id) {
				showPartsPopover(id);
			},
			hidePartsPopover() {
				hidePartsPopover();
			},
			t(scope, options) {
				return i18n.t(scope, options);
			},
			getColorBg(color) {
				return COLOR_MAP[color]?.bg || color;
			},
			getColorFg(color) {
				return COLOR_MAP[color]?.fg || "#fff";
			},
			onPerPageChange(params) {
				window.localStorage.setItem('parts_row_num', params.currentPerPage);
			},
			mycommentChanged(event) {
				fetch('/my_parts', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify({my_part: {part_id: event.target.dataset.id, comment: event.target.value}})}).catch(error => {
					alert("Failed to save!");
				});
			},
			formatMarkdown(s) {
				return s ? marked.parse(s) : s;
			},
			onSortChange(params) {
				this.sortOptions = params[0];
				this.update_rows();
				window.localStorage.setItem('parts_sort', JSON.stringify(this.sortOptions));
			},
			toggleFilters() {
				this.filtersEnabled = !this.filtersEnabled;
				this.columns.forEach(col => {
					if (col.filterOptions) {
						col.filterOptions.enabled = this.filtersEnabled;
					}
				});
				window.localStorage.setItem('parts_filters_enabled', this.filtersEnabled);
			},
			isGrouping() {
				return !['asc', 'desc'].includes(this.sortOptions.type);
			},
		},
	});
	app.use(VueGoodTablePlugin);
	app.mount('#app')
})

