/* eslint no-console: 0 */
// https://borisflesch.github.io/vue-good-table-next

import { createApp } from 'vue/dist/vue.esm-bundler';
import VueGoodTablePlugin from 'vue-good-table-next';
import 'vue-good-table-next/dist/vue-good-table-next.css'
const csvConverter = require('json-2-csv');
import FileSaver from 'file-saver';
import { marked } from 'marked';

document.addEventListener('DOMContentLoaded', () => {
	const app = createApp({
		data() {
			const url = new URL(window.location);
			const rowsPerPage = parseInt(getLocalStorage('parts_row_num', '20'));
			const initialSort = JSON.parse(getLocalStorage("parts_sort", '{"field": "current_number", "type": "none"}'));
			const initialFiltersEnabled = getLocalStorage('parts_filters_enabled', 'false') === 'true';
			const hidden_columns = getPartListHiddenColums();
			const colors = JSON.parse(document.getElementById('app').dataset.colors);
			const languages = JSON.parse(document.getElementById('app').dataset.languages);
			const username = document.querySelector("body").dataset.user;
			return {
				data: {content: [], totalElements: 0},
				columns: [
					{ label: i18n.t('part.image'), field: 'image', sortable: false, width: '100px', tdClass: 'text-center' },
					{ label: i18n.t('part.title'), field: 'part_group.title', filterOptions: {enabled: initialFiltersEnabled} },
					{ label: i18n.t('part.current_number'), field: 'current_number', filterOptions: {enabled: initialFiltersEnabled}, type: 'number', hidden: hidden_columns['current_number'] },
					{ label: i18n.t('part.hist_numbers', { count: 2 }), field: 'hist_numbers', sortable: false, hidden: hidden_columns['hist_numbers'], formatFn: (artvar) => artvar.map(el => `${el.number || "-"} (${el.year})`).join(", ") },
					{ label: i18n.t('part.year'), field: 'year', type: 'number', hidden: hidden_columns['year'] },
					{ label: i18n.t('part.remarks'), field: 'remarks', width: '50ch', filterOptions: {enabled: initialFiltersEnabled}, hidden: hidden_columns['remarks']},
					{ label: i18n.t('part_group.remarks'), field: 'part_group.remarks', width: '50ch', filterOptions: {enabled: initialFiltersEnabled}, hidden: hidden_columns['part_group.remarks']},
					{ label: i18n.t('part.weight'), field: 'part_group.weight', type: 'number', hidden: hidden_columns['part_group.weight'], formatFn: weight => weight?.toLocaleString() },
					{ label: i18n.t('part.colors'), field: 'colors', sortable: false, hidden: hidden_columns['colors'], filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: colors.map(color => ({value: color, text: i18n.t(color, {scope: "colors"})})).sort((a, b) => {return a.text.localeCompare(b.text)})} },
					{ label: i18n.t('part.bio'), field: 'bio', type: 'boolean', filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: [{value: "true", text: i18n.t("true", {scope: "bio"})}, {value: "false", text: i18n.t("false", {scope: "bio"})}]}, hidden: hidden_columns['bio'], formatFn: bio => bio != null ? i18n.t(bio.toString(), {scope: "bio"}) : null },
					{ label: i18n.t('part.language'), field: 'language', hidden: hidden_columns['language'], formatFn: lang => lang != null ? i18n.t(lang, {scope: "languages"}) : null, filterOptions: {enabled: initialFiltersEnabled, filterDropdownItems: languages.map(lang => ({value: lang, text: i18n.t(lang, {scope: "languages"})})).sort((a, b) => {return a.text.localeCompare(b.text)})} },
					{ label: i18n.t('part.length'), field: 'part_group.length', type: 'number', hidden: hidden_columns['part_group.length'], formatFn: length => length?.toLocaleString() },
					... username ? [{ label: i18n.t('part.articleCounts'), field: 'articleCounts', sortable: false, hidden: hidden_columns['articleCounts'], width: '10ch' }] : [],
					... username ? [{ label: i18n.t('part.mycomment'), field: 'mycomment', sortable: false, hidden: hidden_columns['mycomment'] }] : [],
				],
				searchOptions: {
					enabled: true,
					placeholder: i18n.t('part.search.description'),
				},
				paginationOptions: {
					enabled: true,
					perPage: rowsPerPage,
					mode: 'pages',
					dropdownAllowAll: false,
					jumpFirstOrLast: true,
					perPageDropdown: [10, 20, 50, 100],
				},
				pageOptions: {currentPage: 1, currentPerPage: rowsPerPage},
				sortOptions: initialSort,
				filters: [],
				isLoading: true,
				lastSearchTerm: url.searchParams.get("keyword") || url.searchParams.get("fulltext")?.replace(/["]+/g, '') || "",
				category: parseInt(document.getElementById('app').dataset.category) || undefined,
				filtersEnabled: initialFiltersEnabled,
				myCommentOptions: [...document.querySelectorAll("#myCommentOptions option")].map(opt => opt.value),
			}
		},
		created() {
			if (!this.columns.map(col => col.field).includes(this.sortOptions.field)) {
				// reset to default sort column if field is not in columns
				this.sortOptions.field = "year";
			}
		},
		mounted() {
			this.$refs["part-table"].globalSearchTerm = this.lastSearchTerm;
			this.updateCatLinks();
		},
		updated() {
			$(".myComment").autocomplete({
				source: this.myCommentOptions
			});
			$(".myComment").removeClass("myComment"); // init only once
		},
		methods: {
			fetchData(size, cb) {
				const url = new URL(window.location.href);
				url.pathname = "/parts"
				url.searchParams.append('searchtext', this.lastSearchTerm);
				url.searchParams.append('searchcolums', JSON.stringify(this.columns.filter(col => !col.hidden).map(col => col.field))); // only visible columns
				url.searchParams.append('page', this.pageOptions.currentPage);
				url.searchParams.append('size', size !== undefined ? size : this.pageOptions.currentPerPage);
				url.searchParams.append('sortBy0', this.sortOptions.field);
				if (['asc', 'desc'].includes(this.sortOptions.type)) {
					url.searchParams.append('sortAsc0', this.sortOptions.type === 'asc');
				}
				if (this.filtersEnabled) {
					url.searchParams.append('filters', JSON.stringify(this.filters));
				}
				url.searchParams.append('include_my_parts', true);
				if (this.category) {
					url.searchParams.append('category', this.category);
				}
				fetch(url.href, {headers: {'Accept': 'application/json'}}).then(response => response.json()).then(data => {
					cb(data);
				}).catch(error => {
					console.error(error);
					cb(undefined, error);
				});
			},
			fetchDataTable() {
				this.isLoading = true;
				const searchTerm = this.lastSearchTerm;
				this.fetchData(undefined, (res, err) => {
					if (searchTerm === this.lastSearchTerm) {
						this.isLoading = false;
						if (this.isGrouping()) {
							// Group result by category
							const contentByCat = res.content.reduce((acc,row) => {
								(acc[row.part_group.category.full_title] = acc[row.part_group.category.full_title] || []).push(row);
								return acc;
							}, {})
							const content = Object.keys(contentByCat).map((key) => ({
								mode: 'span',
								label: key,
								children: contentByCat[key],
							}));
							this.data = {...res, content: content};
						} else {
							this.data = res;
						}
					}
				});
			},
			isGrouping() {
				return !['asc', 'desc'].includes(this.sortOptions.type);
			},
			updateCatLinks() {
				document.querySelectorAll(".catlink").forEach(catlink => {
					catlink.href = catlink.dataset.orighref + (this.lastSearchTerm ? ('&keyword=' + this.lastSearchTerm) : '');
				});
			},
			search({searchTerm}) {
				this.pageOptions.currentPage = 1; // page to page 1 of search changed
				this.lastSearchTerm = searchTerm;
				this.fetchDataTable();
				this.updateCatLinks();
			},
			onPageChange(params) {
				this.pageOptions = params;
				this.fetchDataTable();
			},
			onPerPageChange(params) {
				this.pageOptions = params;
				this.fetchDataTable();
				window.localStorage.setItem('parts_row_num', params.currentPerPage);
			},
			onSortChange(params) {
				this.sortOptions = params[0];
				this.fetchDataTable();
				window.localStorage.setItem('parts_sort', JSON.stringify(this.sortOptions));
			},
			onColumnFilter(params) {
				this.filters = params.columnFilters;
				this.fetchDataTable();
			},
			toggleColumn(index, event) {
				this.columns[index].hidden = !this.columns[index].hidden;
				this.fetchDataTable(); // we search on all visible fields, so update results if visible fields change
				const hidden_columns = this.columns.reduce((acc, col) => {acc[col.field] = !!col.hidden; return acc;}, {});
				window.localStorage.setItem('parts_hidden_columns', JSON.stringify(hidden_columns));
			},
			downloadCsv() {
				this.fetchData(-1, (res, err) => {
					csvConverter.json2csv(res.content, (err, csv) => {
						if (err) {
							console.error(err);
						} else {
							const blob = new Blob([csv], {type: "text/plain;charset=utf-8"});
							saveAs(blob, "parts_export.csv");
						}
					}, {expandArrayObjects: true, emptyFieldValue: '', keys: ['id', {field: 'current_number', title: 'number'}, {field: 'part_group.title', title: 'title'}, {field: 'part_group.category.full_title', title: 'category'}, 'year', 'color1', 'color2', 'color3', 'language', 'remarks', {field: 'part_group.weight', title: 'weight'}, {field: 'part_group.length', title: 'length'}, {field: 'part_group.remarks', title: 'remarks (Group)'}]});
				});
			},
			toggleFilters() {
				this.filtersEnabled = !this.filtersEnabled;
				this.columns.forEach(col => {
					if (col.filterOptions) {
						col.filterOptions.enabled = this.filtersEnabled;
					}
				});
				window.localStorage.setItem('parts_filters_enabled', this.filtersEnabled);
			},
			showPartsPopover(id) {
				showPartsPopover(id);
			},
			hidePartsPopover() {
				hidePartsPopover();
			},
			t(scope, options) {
				return i18n.t(scope, options);
			},
			getColorBg(color) {
				return COLOR_MAP[color]?.bg || color;
			},
			getColorFg(color) {
				return COLOR_MAP[color]?.fg || "#fff";
			},
			articleCountsChanged(event) {
				fetch('/my_parts', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify({my_part: {part_id: event.target.dataset.id, count: event.target.value}})}).catch(error => {
					alert("Failed to save!");
				});
			},
			mycommentChanged(event) {
				const comment = event.target.value;
				if (comment && !this.myCommentOptions.includes(comment)) {
					this.myCommentOptions.push(comment);
				}
				fetch('/my_parts', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify({my_part: {part_id: event.target.dataset.id, comment: comment}})}).catch(error => {
					alert("Failed to save!");
				});
			},
			formatMarkdown(s) {
				return s ? marked.parse(s) : s;
			},
		},
	});
	app.use(VueGoodTablePlugin);
	app.mount('#app')
})
