require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest
	test "route test" do
		assert_generates "details.php", { :controller => "parts", :action => "showsfpartsid" }
		assert_generates "ft-article/995", { :controller => "parts", :action => "showFtdb30TicketNumber", :id => "995" }
		#assert_generates "search.php", { :controller => "parts", :action => "index" }
		#assert_generates "tickets", { :controller => "parts", :action => "index" }
	end
end
