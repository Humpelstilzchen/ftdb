FROM ruby:3.3.6-alpine3.21 AS build

ARG SECRET_KEY_BASE
ENV SECRET_KEY_BASE=$SECRET_KEY_BASE

ENV RAILS_ENV=production BUNDLE_APP_CONFIG=/srv/app/.bundle

RUN apk add --no-cache build-base libpq-dev imagemagick-libs vips tzdata

WORKDIR /srv/app

COPY . .
RUN chmod +x docker/app.entrypoint.sh

EXPOSE 80

ENTRYPOINT ["docker/app.entrypoint.sh"]