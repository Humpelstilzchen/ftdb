FROM caddy:2.9-alpine

COPY docker/Caddyfile /etc/caddy/Caddyfile

COPY public /srv/app