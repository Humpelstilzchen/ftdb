# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
	# Specify a root folder where Swagger JSON files are generated
	# NOTE: If you're using the rswag-api to serve API descriptions, you'll need
	# to ensure that it's configured to serve Swagger from the same folder
	config.openapi_root = Rails.root.join('swagger').to_s

	# Define one or more Swagger documents and provide global metadata for each one
	# When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
	# be generated at the provided relative path under openapi_root
	# By default, the operations defined in spec files are added to the first
	# document below. You can override this behavior by adding a openapi_spec tag to the
	# the root example_group in your specs, e.g. describe '...', openapi_spec: 'v2/swagger.json'
	config.openapi_specs = {
		'v1/swagger.yaml' => {
			openapi: '3.0.1',
			info: {
				title: 'API V1',
				version: 'v1'
			},
			paths: {},
			servers: [
				{
					url: '{defaultHost}',
					variables: {
						defaultHost: {
							default: 'https://ft-datenbank.de'
						}
					}
				}
			],
			components: {
				schemas: {
					part: {
						type: :object,
						properties: {
							id: { type: :integer },
							current_number: { type: :string, example: "123456" },
							color1: { type: :string, example: "black" },
							color2: { type: :string, example: "red" },
							color3: { type: :string, example: "" },
							colors: { type: :array, items: { type: 'string', example: "black" } },
							bio: { type: :boolean, example: false },
							language: { type: :string, example: "de" },
							year: { type: :integer, example: 1990 },
							sfpartsid: { type: :string, example: "" },
							changedate: { type: :string, example: "2024-12-07T09:47:22.599Z" },
							hist_numbers: { type: :array, items: { '$ref' => '#/components/schemas/hist_number' } },
							remarks: { type: :string, example: "" },
							part_group: { '$ref' => '#/components/schemas/part_group' },
							ftdb30TicketNumber: { type: :integer, example: 1234 },
							image: { type: :string, example: "path/to/image" },
							my_part: { '$ref' => '#/components/schemas/my_part' }
						},
						required: [ 'id' ]
					},
					category: {
						type: :object,
						properties: {
							id: { type: :integer },
							full_title: { type: :string, example: "Einzelteile / Bausteine + Gelenke / Baustein" },
							sfpartsid: { type: :string, example: "" },
							remarks: { type: :string, example: "" },
						},
						required: [ 'id', 'full_title' ]
					},
					hist_number: {
						type: 'object',
						properties: {
							number: { type: :string, example: "123456" },
							year: { type: :integer, example: 1990 },
						},
						required: [ 'year' ]
					},
					part_group: {
						type: :object, properties: {
							title: { type: :string, example: "Baustein 30" },
							remarks: { type: :string, example: "" },
							weight: { type: :number },
							length: { type: :number },
							category: { '$ref' => '#/components/schemas/category' }
						},
						required: [ 'title' ]
					},
					my_part: {
						type: :object, properties: {
							count: { type: :number, example: "4" },
							comment: { type: :string, example: "Locker 1 Slot 4" },
						},
						description: "Own inventory, only included if login cookie is provided."
					},
				}
			},
		}
	}

	# Specify the format of the output Swagger file when running 'rswag:specs:swaggerize'.
	# The openapi_specs configuration option has the filename including format in
	# the key, this may want to be changed to avoid putting yaml in json files.
	# Defaults to json. Accepts ':json' and ':yaml'.
	config.openapi_format = :yaml
end
