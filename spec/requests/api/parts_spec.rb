require 'swagger_helper'

RSpec.describe 'api/parts', type: :request do
	path '/parts' do
		get 'Retrieves parts' do
			tags 'Parts'
			produces 'application/json'
			parameter name: :searchtext, in: :query, type: :string
			parameter name: :searchcolums, in: :query, type: :string, description: "Columns to search in as json array", example: "[part_group.title, current_number]"
			parameter name: :filters, in: :query, type: :string, description: "Pass filters as json object {fieldname: 'value to filter for'}"
			parameter name: :category, in: :query, type: :integer, description: "Category id to filter for"
			parameter name: :include_my_parts, in: :query, type: :boolean, description: "When _oauth2_proxy Cookie is sent and this parameter is true the user part count and comment is included. Defaults to false"
			parameter name: :page, in: :query, type: :string
			parameter name: :size, in: :query, type: :string, example: "100"
			parameter name: :sortBy0, in: :query, type: :string
			parameter name: :sortAsc0, in: :query, type: :string, description: "'true' for asc, 'false' for desc. Default unsorted."

			response '200', 'found' do
				schema type: :object,
					properties: {
						content: {
							type: :array,
							items: { '$ref' => '#/components/schemas/part' }
						},
						totalElements: {type: :integer},
					},
					required: [ 'totalElements', 'content' ]

				run_test!
			end

			response '404', 'not found' do
				let(:id) { 'invalid' }
				run_test!
			end
		end
	end

	path '/parts/{id}/partlist' do
		get 'Retrieves list of parts of a kit' do
			tags 'Parts'
			produces 'application/json'
			parameter name: :id, in: :path, type: :string

			response '200', 'found' do
				schema type: :array, items: {allOf: [ {'$ref' => '#/components/schemas/part'}, { type: :object, properties: {amount: {type: :integer}}} ] }
				let(:id) { Part.create!({current_number: "1234", part_group: PartGroup.create({title: "Group1"})}).id }
				run_test!
			end

			response '404', 'not found' do
				let(:id) { -1 }
				run_test!
			end
		end
	end

	path '/parts/{id}/variants' do
		get 'Retrieves variants of a part' do
			tags 'Parts'
			produces 'application/json'
			parameter name: :id, in: :path, type: :string

			response '200', 'found' do
				schema type: :array, items: { '$ref' => '#/components/schemas/part' }
				let(:id) { Part.create!({current_number: "1234", part_group: PartGroup.create({title: "Group1"})}).id }
				run_test!
			end

			response '404', 'not found' do
				let(:id) { -1 }
				run_test!
			end
		end
	end

	path '/parts/{id}/contained' do
		get 'Retrieves kits this part is contained in' do
			tags 'Parts'
			produces 'application/json'
			parameter name: :id, in: :path, type: :string

			response '200', 'found' do
				schema type: :array, items: {allOf: [ {'$ref' => '#/components/schemas/part'}, { type: :object, properties: {amount: {type: :integer}}} ] }
				let(:id) { Part.create!({current_number: "1234", part_group: PartGroup.create({title: "Group1"})}).id }
				run_test!
			end

			response '404', 'not found' do
				let(:id) { -1 }
				run_test!
			end
		end
	end
end
