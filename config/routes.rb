Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # Routes for compatibility with old sites
  match 'details.php' => 'parts#showsfpartsid', via: :get
  match 'ArticleVariant.aspx' => 'parts#showsfpartsid', via: :get
  match 'ft-article/:id' => 'parts#showFtdb30TicketNumber', via: :get
  match 'search.php' => 'parts#index', via: :get
  match 'tickets' => 'parts#index', via: :get
  match 'binary/:id' => 'parts#attachmentFtdb30TicketNumber', via: :get
  match 'categories.php' => 'categories#showsfpartsid', via: :get
  match 'parts/partlistFtdb30TicketNumber/:id' => 'parts#partlistFtdb30TicketNumber', via: :get

  resources :parts do
    get :overlay
    get :partlist
    get :variants
    get :contained
    get :newpartgroup
    get "showhist/:idx", to: "parts#showhist"
    get "partlisthist/:idx", to: "parts#partlisthist"
    delete "restore/:idx", to: "parts#restore"
  end
  resources :categories

  resources :my_parts

  root to: 'parts#index'

  get "/about", to: "application#about"
  get "/token_refresh", to: "application#token_refresh"
end
