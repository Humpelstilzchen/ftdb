class CreateMyParts < ActiveRecord::Migration[7.1]
  def change
    create_table :my_parts do |t|
      t.integer :user_id
      t.integer :part_id
      t.integer :count

      t.timestamps
    end
  end
end
