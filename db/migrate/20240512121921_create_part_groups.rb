class CreatePartGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :part_groups do |t|
      t.string :title
      t.float :weight
      t.text :remarks
      t.string :sfpartsid
      t.integer :category_id

      t.timestamps
    end
  end
end
