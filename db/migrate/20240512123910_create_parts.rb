class CreateParts < ActiveRecord::Migration[6.1]
  def change
    create_table :parts do |t|
      t.integer :part_group_id
      t.integer :year
      t.integer :color1
      t.integer :color2
      t.integer :language
      t.text :remarks
      t.string :current_number
      t.string :sfpartsid
      t.integer :ftdb30TicketNumber

      t.timestamps
    end
  end
end
