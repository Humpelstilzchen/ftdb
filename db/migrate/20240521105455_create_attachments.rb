class CreateAttachments < ActiveRecord::Migration[7.1]
  def change
    create_table :attachments do |t|
      t.integer :part_id
      t.string :ftdb30TicketNumber
      t.string :title

      t.timestamps
    end
  end
end
