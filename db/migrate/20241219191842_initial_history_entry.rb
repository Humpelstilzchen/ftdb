class InitialHistoryEntry < ActiveRecord::Migration[7.2]
	def change
		Part.all.each do |part|
			part.create_snapshot!(identifier: "initial")
		end
	end
end
