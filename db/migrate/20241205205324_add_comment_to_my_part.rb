class AddCommentToMyPart < ActiveRecord::Migration[7.2]
  def change
    add_column :my_parts, :comment, :text
  end
end
