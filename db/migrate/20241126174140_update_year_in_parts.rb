class UpdateYearInParts < ActiveRecord::Migration[7.2]
	def change
		Part.all.each do |part|
			if part.hist_numbers.length == 0 and !part.year.nil?
				p "Part #{part.id} create hist number for year #{part.year}"
				part.hist_numbers.build().update!({year: part.year, number: nil})
			end
			if part.hist_numbers.length > 0
				new_year = part.hist_numbers.first&.year
				if !part.year.nil? and part.year != new_year
					# old year is different, back it up
					if !part.remarks.blank?
						part.remarks += "\r\n"
					else
						part.remarks = ""
					end
					part.remarks += "Jahr (Importiert): #{part.year}"
					p "Part #{part.id} year was #{part.year}, setting to #{new_year}"
				end
				part.save
			end
		end
	end
end
