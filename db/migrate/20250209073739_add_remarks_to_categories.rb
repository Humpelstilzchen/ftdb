class AddRemarksToCategories < ActiveRecord::Migration[7.2]
  def change
    add_column :categories, :remarks, :text
  end
end
