class AddIndexToCategory < ActiveRecord::Migration[7.2]
  def change
    add_index :categories, [:parent_id, :title], unique: true
  end
end
