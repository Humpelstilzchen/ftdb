class CreatePartListEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :part_list_entries do |t|
      t.integer :part_id
      t.integer :item_id
      t.integer :amount

      t.timestamps
    end
  end
end
