class AddImageToParts < ActiveRecord::Migration[7.1]
  def change
    add_column :parts, :image_id, :integer
  end
end
