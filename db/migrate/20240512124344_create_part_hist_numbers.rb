class CreatePartHistNumbers < ActiveRecord::Migration[6.1]
  def change
    create_table :part_hist_numbers do |t|
      t.integer :part_id
      t.integer :year
      t.string :number

      t.timestamps
    end
  end
end
