class AddColor3ToParts < ActiveRecord::Migration[7.1]
  def change
    add_column :parts, :color3, :integer
  end
end
