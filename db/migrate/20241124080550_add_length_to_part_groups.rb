class AddLengthToPartGroups < ActiveRecord::Migration[7.2]
	def up
		add_column :part_groups, :length, :float

		PartGroup.all.each do |pg|
			if (pg.title.downcase.include? 'strebe' or pg.title.downcase.include? 'achse') and !pg.title.include? '+' and !pg.title.downcase.include? 'zahnrad' and !pg.title.downcase.include? 'schnecke' and !pg.title.downcase.include? 'ritzel' and !pg.title.include? '&'
				length = pg.title.match(/([\d,]+)/)&.captures&.first&.sub(',', '.')&.to_f
				if length
					p "#{pg.title} length=#{length}"
					pg.update(length: length)
				end
			end
		end
	end

	def down
		remove_column :part_groups, :length
	end
end
