class CreateCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :categories do |t|
      t.string :title
      t.integer :parent_id
      t.string :sfpartsid

      t.timestamps
    end
  end
end
